/*
* app_Strobe.c
*
* Created: 25/08/2019 11:17:08 p. m.
*  Author: OmarSevilla
*/

#include "app_STROBESeq.h"

/* Private typedef*/
typedef enum
{
	STROBE1,
	STROBE2,
	N_STROBE
}T_STROBE_SEQ;

#define STROBE_SEQ_TOGGLE_TIME		(30u)	//100ms

#define STROBE_SEQ_STROBE_CYCLES	(15u)	//10 cycles per strobe channel


/* Private Variables */
static unsigned char rub_StrobeEnable;
static unsigned char rub_StrobeCount;
static unsigned int ruw_StrobeSeqCounter;
static T_STROBE_SEQ re_StrobeSeq;

/* Public Variables */
unsigned char rub_STROBETick;

/******************* Public Functions ***********************/

/******************************************************
* Function Name: app_STROBESeqInit
* Description: This function init the STROBE Seq module
* Parameters: void
* Return: void
*******************************************************/
void app_STROBESeqInit(void)
{
	/* Clear Tick */
	rub_STROBETick =false;
	/* Default value for strobe */
	rub_StrobeEnable = false;
	/* Clear Seq Counter */
	ruw_StrobeSeqCounter = 0u;
	/* Clear Strobe Count */
	rub_StrobeCount = 0u;
}

/******************************************************
* Function Name: app_STROBESeqTask
* Description: This function execute Strobe sequence if enable.
This function shall called periodically
* Parameters: void
* Return: void
*******************************************************/
void app_STROBESeqTask(void)
{
	if(false != rub_StrobeEnable)
	{//Strobe seq enable
		
		if(rub_STROBETick)
		{//Tick available
			
			//Tick Consume
			rub_STROBETick = false;
			
			/* Check Sequence Counter */
			if (ruw_StrobeSeqCounter >= STROBE_SEQ_TOGGLE_TIME)
			{
				//Restart sequence counter
				ruw_StrobeSeqCounter = 0u;
				
				/* STROBE Seq SM */
				switch (re_StrobeSeq)
				{
					default:
					case STROBE1:
					{
						/* Toggle output */
						STROBESEQ_TOGGLE_STROBE1_PIN;
						STROBESEQ_CLEAR_STROBE2_PIN;
						
						/* Check if STROBE 1 is finished */
						if(rub_StrobeCount >= STROBE_SEQ_STROBE_CYCLES)
						{//Strobe 1 finish
							/* Clear strobe count */
							rub_StrobeCount = 0u;
							/* Prepare next channel */
							re_StrobeSeq = STROBE2;
						}
						else
						{
							/* Increase Strobe Count */
							rub_StrobeCount++;
						}
						
					}break;
					
					case STROBE2:
					{
						/* Toggle output */
						STROBESEQ_TOGGLE_STROBE2_PIN;
						STROBESEQ_CLEAR_STROBE1_PIN;
						
						/* Check if STROBE 2 is finished */
						if(rub_StrobeCount >= STROBE_SEQ_STROBE_CYCLES)
						{//Strobe 2 finish
							/* Clear strobe count */
							rub_StrobeCount = 0u;
							/* Prepare next channel */
							re_StrobeSeq = STROBE1;
						}
						else
						{
							/* Increase Strobe Count */
							rub_StrobeCount++;
						}
					}break;
				}
			}
			else
			{
				//Increase Sequence counter
				ruw_StrobeSeqCounter++;
			}
		}
		else
		{//Tick not avaliable
			/* Do nothing */
		}
	}
	else
	{//Strobe seq disable
		
		STROBESEQ_CLEAR_STROBE1_PIN;
		STROBESEQ_CLEAR_STROBE2_PIN;
		//Clear seq counter
		ruw_StrobeSeqCounter = 0u;
		/* Clear Strobe Count */
		rub_StrobeCount = 0u;
	}
}

/******************************************************
* Function Name: app_STROBESeqCtrl
* Description: Activate or disable strobe
* Parameters: void
* Return: void
*******************************************************/
void app_STROBESeqCtrl(void)
{
	/* Check Strobe status */
	if(rub_StrobeEnable != true)
	{//Strobe disable
		//Enable strobe
		rub_StrobeEnable = true;
	}
	else
	{//Strobe enabled
		//Disable strobe
		rub_StrobeEnable = false;
	}
}