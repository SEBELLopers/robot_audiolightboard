#include <atmel_start.h>
#include "app_LEDSeq.h"
#include "app_STROBESeq.h"
#include "app_LASERSeq.h"
#include "app_MUTE.h"
#include "app_Debounce.h"
#include "app_SWPWM.h"



void cbk_Button0(T_BUTTON_STS l_ButtonID_Sts){
	if (l_ButtonID_Sts == PRESSED)
	{
		app_LEDNextSeq();
	}
	else
	{
		/* Do nothing */
	}
}
void cbk_Button1(T_BUTTON_STS l_ButtonID_Sts){
	if (l_ButtonID_Sts == PRESSED)
	{
		app_STROBESeqCtrl();
	}
	else
	{
		/* Do nothing */
	}
}
void cbk_Button2(T_BUTTON_STS l_ButtonID_Sts){
	if (l_ButtonID_Sts == PRESSED)
	{
		app_LASERSeqCtrl();
	}
	else
	{
		/* Do nothing */
	}
}
void cbk_Button3(T_BUTTON_STS l_ButtonID_Sts){
	if (l_ButtonID_Sts == PRESSED)
	{
		app_MUTECtrl();
	}
	else
	{
		/* Do nothing */
	}
}

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	
	/* APP Init tasks */
	app_SWPWMInit();
	app_LEDSeqInit();
	app_STROBESeqInit();
	app_LASERSeqInit();
	app_MUTEInit();

	Enable_global_interrupt();
	/* Replace with your application code */
	while (1) {
		/* LED Task */
		app_LEDSeqTask();
		/* STROBE Task */
		app_STROBESeqTask();
		/* LASER Task */
		app_LASERSeqTask();
		/* MUTE Task */
		app_MUTETask();
		/* Debounce Task */
		app_Debounce_Task();
	}
}
