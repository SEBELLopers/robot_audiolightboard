/*
 * app_Debounce.c
 *
 *  Created on: 28 ago 2019
 *      Author: uidv7995
 */
#include "app_Debounce.h"
#include "atmel_start_pins.h"



/********************************************************************************************
 *Declaration of types utilized for Internal modules
 ********************************************************************************************/
typedef struct{
	unsigned char rub_ButtonPINID;
	T_BUTTON_STS re_buttonState;
	unsigned int ruw_DebounceTime;
	unsigned int ruw_ElapsedDebounceTime;
#ifdef APP_DBNC_LONGPRESS
	unsigned int ruw_LongPress_Time;
#ifdef APP_DBNC_VARIABLELONGPRESS
	unsigned int ruw_ElapsedLongPress_Time;
#endif/*APP_DBNC_VARIABLELONGPRESS*/

#endif/*APP_DBNC_LONGPRESS*/

#ifdef 	APP_DBNC_CLBCK_ENABLED
	void (* btn_cbk)(T_BUTTON_STS);
#endif/*APP_DBNC_CLBCK_ENABLED*/
}T_BUTTON; /*The Button states and characteristics*/


/********************************************************************************************
 *Configuration of the Button Debounce
 ********************************************************************************************/

/**Button Array Declaration*/
static T_BUTTON r_ButtonList[NB_BUTTONS] = {

		{/*Initializing Button 0*/
			0,
			IDLE,
			5,  /*Debounce time = 100ms  (Tick * Count)*/
			0,
			200,/*LongPress time = 2s  (Tick * Count)*/
			0,
			&cbk_Button0
		},
		{/*Initializing Button 1*/
			0,
			IDLE,
			5,
			0,
			200,
			0,
			&cbk_Button1
		},
		{/*Initializing Button 2*/
			0,
			IDLE,
			5,
			0,
			200,
			0,
			&cbk_Button2
		},
		{/*Initializing Button 3*/
			0,
			IDLE,
			5,
			0,
			200,
			0,
			&cbk_Button3
		}

};

unsigned char r_taskTickCounter= 0x0; /*Variable that provides Tick count to execute the task*/

/********************************************************************************************
 *Prototypes
 ********************************************************************************************/

void app_Debounce_MainFunction(void);
void app_Debounce_Set_Status(T_BUTTON_ID l_ButtonID, T_BUTTON_STS l_ButtonStatusToSet);
unsigned char app_Debounce_Get_Pin_Level(T_BUTTON_ID l_ButtonID);
/********************************************************************************************
 *Declaration of External Functions
 ********************************************************************************************/



T_BUTTON_STS Get_Button_Status(T_BUTTON_ID l_ButtonID){
	T_BUTTON_STS l_Button_Status = IDLE;

	if(l_ButtonID < NB_BUTTONS){
		/*The Button ID is whithin the range*/
		l_Button_Status = r_ButtonList[l_ButtonID].re_buttonState;
	}else{
		/*The Button ID is whithin is not in range retunr an error Note: The user is responsible of checking if the interface returned a valid value*/
		l_Button_Status =NOK_INVALID_BUTTON_ID;
	}
	return l_Button_Status;
}



/********************************************************************************************
 *Declaration of Internal Functions
 ********************************************************************************************/



/********************************************************************************************
 *Function: app_Debounce_Task
 *Description: This functions is called internally by the module to determine the Buttons Status
 * the call period must be 10ms
 ********************************************************************************************/
void app_Debounce_Task(void){
	unsigned char l_ButtonID_Iterator = BUTTON_0; /*Set the first Button*/
	unsigned char l_ButtonPinStatus = 0x1u; /*Variable the stores the Pin STATUS*/
	
		
	if(r_taskTickCounter>=10){	
			/*TICK of 10 ms required*/
			r_taskTickCounter = 0x0u; /*Clear the counter for the ticks to star a new waiting time*/
			if(NB_BUTTONS != 0){
			/**Button Present execute the process**/
			for(;l_ButtonID_Iterator < NB_BUTTONS;l_ButtonID_Iterator++){

				l_ButtonPinStatus = app_Debounce_Get_Pin_Level( l_ButtonID_Iterator);/*Get PIN status*/
						switch(Get_Button_Status(l_ButtonID_Iterator)){
							case IDLE:
								if(PIN_ACTIVE_INVERSE == l_ButtonPinStatus){
									/*The Pin is Active and we are in IDLE go to Debouncing*/
									/*The Pin is active but currently is IDLE therefore do a debounce*/
									app_Debounce_Set_Status(l_ButtonID_Iterator, DEBOUNCING);
								}else{
									/*Do nothing we are already in IDLE*/
								}
							
							break;
							case DEBOUNCING:
								if(r_ButtonList[l_ButtonID_Iterator].ruw_ElapsedDebounceTime>=r_ButtonList[l_ButtonID_Iterator].ruw_DebounceTime){
									if(PIN_ACTIVE_INVERSE == l_ButtonPinStatus){
									/*The debounce has elapsed we can go to PRESSED*/
										/*The Button has succesfully passed the debounce time and is on active state*/
										app_Debounce_Set_Status(l_ButtonID_Iterator, PRESSED);
									}else{
										/*The Button is not active go to IDLE*/
										app_Debounce_Set_Status(l_ButtonID_Iterator, IDLE);
									}/*if(PIN_ACTIVE_INVERSE == l_ButtonPinStatus)*/

								}else{
									/*The button is still not debounced keep waiting*/
								r_ButtonList[l_ButtonID_Iterator].ruw_ElapsedDebounceTime = r_ButtonList[l_ButtonID_Iterator].ruw_ElapsedDebounceTime+1; /*Increment the Timer by 1*/
								}/*if(r_ButtonList[l_ButtonID_Iterator].ruw_ElapsedDebounceTime>=r_ButtonList[l_ButtonID_Iterator].ruw_DebounceTime)*/
							break;
							
							case PRESSED:
								if(PIN_ACTIVE_INVERSE == l_ButtonPinStatus){
									/*Pin is still active keep counting for long press*/
									if(r_ButtonList[l_ButtonID_Iterator].ruw_ElapsedLongPress_Time>=r_ButtonList[l_ButtonID_Iterator].ruw_LongPress_Time){
										/*The button is now passed the Long Press Time*/
										app_Debounce_Set_Status(l_ButtonID_Iterator, LONGPRESSED);
									}else{
										/*The button is still not in longpress but still actvive keep waiting*/
										r_ButtonList[l_ButtonID_Iterator].ruw_ElapsedLongPress_Time = r_ButtonList[l_ButtonID_Iterator].ruw_ElapsedLongPress_Time+1; /*Increment the Timer by 1*/
									}/*if(r_ButtonList[l_ButtonID_Iterator].ruw_ElapsedLongPress_Time>=r_ButtonList[l_ButtonID_Iterator].ruw_LongPress_Time)*/
						
								}else{
									/*The Pin is now down go to IDLE*/
									app_Debounce_Set_Status(l_ButtonID_Iterator, IDLE);
								}/*if(PIN_ACTIVE_INVERSE == l_ButtonPinStatus)*/
							break;
							
							case LONGPRESSED:
							if(PIN_ACTIVE_INVERSE != l_ButtonPinStatus){
								/*Go to IDLE we are not longer active*/
								app_Debounce_Set_Status(l_ButtonID_Iterator, IDLE);
							}else{
								/*Do nothing we are active and in Longpress*/
							}/*if(PIN_ACTIVE_INVERSE != l_ButtonPinStatus)*/
							
							break;
							
							default:
							break;
						}/* switch(Get_Button_Status(l_ButtonID_Iterator))*/
				
			}/*for(;l_ButtonID_Iterator < NB_BUTTONS;l_ButtonID_Iterator++)*/

		}else
		{
		/*Do nothing since there are no buttons present on the configuration*/
		}/*if(NB_BUTTONS != 0)*/
	}else{
		
		   }/*if(r_taskTickCounter>=10)*/
		
}



/********************************************************************************************
 *Function: app_Debounce_Set_Status
 *Description: This functions is called internally by the module to determine the Buttons Status
 * the call period must be 10ms
 ********************************************************************************************/

void app_Debounce_Set_Status(T_BUTTON_ID l_ButtonID, T_BUTTON_STS l_ButtonStatusToSet){




	if(l_ButtonID < NB_BUTTONS){
		/*Evaluate the Status Request*/
		switch(l_ButtonStatusToSet){
		case DEBOUNCING:
			r_ButtonList[l_ButtonID].re_buttonState = DEBOUNCING; /*Set the state of the Button to be debounced*/
			r_ButtonList[l_ButtonID].ruw_ElapsedDebounceTime = 0x0; /*Ensure that the timer starts at zer0*/
			break;
		case PRESSED:
			r_ButtonList[l_ButtonID].re_buttonState = PRESSED; /*Set the state of the Button to be PRESSED*/
			r_ButtonList[l_ButtonID].ruw_ElapsedLongPress_Time = 0x0; /*Ensure that the timer starts at zer0*/
			#ifdef  APP_DBNC_CLBCK_ENABLED
			(*r_ButtonList[l_ButtonID].btn_cbk)(PRESSED); /*Call the CAllback function*/
			#endif
			break;
		case LONGPRESSED:
			r_ButtonList[l_ButtonID].re_buttonState = LONGPRESSED; /*Set the state of the Button to be LONGPRESSED*/
			#ifdef  APP_DBNC_CLBCK_ENABLED
			(*r_ButtonList[l_ButtonID].btn_cbk)(LONGPRESSED); /*Call the CAllback function*/
			#endif
			break;
		case IDLE:
			r_ButtonList[l_ButtonID].re_buttonState =IDLE;
			#ifdef  APP_DBNC_CLBCK_ENABLED
			(*r_ButtonList[l_ButtonID].btn_cbk)(IDLE); /*Call the CAllback function*/
			#endif
			break;
		default:
		/*Invalid satus request do nothing*/
		break;
		}
		}else{
			/*The Button ID is whithin is not in range*/

		}




}

/********************************************************************************************
 *Function: app_Debounce_Get_Pin_Level
 *Description: This functions is called internally by the module to determine the Buttons Status
 * the call period must be 10ms
 ********************************************************************************************/

unsigned char app_Debounce_Get_Pin_Level(T_BUTTON_ID l_ButtonID){
	unsigned char l_ButtonPinStatus = 0x0u; /*Variable to store the Pin value*/
	if(l_ButtonID < NB_BUTTONS){
	switch(l_ButtonID){
	case BUTTON_0:
		l_ButtonPinStatus = (unsigned char) BUTTON_0_get_level();
		break;
	case BUTTON_1:
		l_ButtonPinStatus = (unsigned char) BUTTON_1_get_level();
		break;
	case BUTTON_2:
		l_ButtonPinStatus = (unsigned char) BUTTON_2_get_level();
		break;
	case BUTTON_3:
		l_ButtonPinStatus = (unsigned char) BUTTON_3_get_level();
		break;
	default:
		break;
	}
	}else{
		/*The Button ID is whithin is not in range*/
		l_ButtonPinStatus= (unsigned char) NOK_INVALID_BUTTON_ID;
	}
	return l_ButtonPinStatus;
}





