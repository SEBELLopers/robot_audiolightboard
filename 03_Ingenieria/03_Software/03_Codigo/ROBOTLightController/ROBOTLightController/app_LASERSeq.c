/*
 * app_LASERSeq.c
 *
 * Created: 25/08/2019 11:50:23 p. m.
 *  Author: OmarSevilla
 */ 

#include "app_LASERSeq.h"

/* Private Variables */
static unsigned char rub_LASEREnable;

/******************* Public Functions ***********************/

/******************************************************
* Function Name: app_LASERSeqInit
* Description: This function init the LASER Seq module
* Parameters: void
* Return: void
*******************************************************/
void app_LASERSeqInit(void)
{
	/* Default value for strobe */
	rub_LASEREnable = false;
}

/******************************************************
* Function Name: app_LASERSeqTask
* Description: This function execute LASER sequence if enable.
This function shall called periodically
* Parameters: void
* Return: void
*******************************************************/
void app_LASERSeqTask(void)
{
	if(false != rub_LASEREnable)
	{//LASER seq enable
		LASERSEQ_SET_LASER_PIN;
	}
	else
	{//LASER seq disable
		LASERSEQ_CLEAR_LASER_PIN;
	}
}

/******************************************************
* Function Name: app_LASERSeqCtrl
* Description: Activate or disable LASER
* Parameters: void
* Return: void
*******************************************************/
void app_LASERSeqCtrl(void)
{
	/* Check LASER status */
	if(rub_LASEREnable != true)
	{//LASER disable
		//Enable LASER
		rub_LASEREnable = true;
	}
	else
	{//LASER enabled
		//Disable LASER
		rub_LASEREnable = false;
	}
}