/*
 * app_MUTE.h
 *
 * Created: 25/08/2019 11:57:46 p. m.
 *  Author: OmarSevilla
 */ 


#ifndef APP_MUTE_H_
#define APP_MUTE_H_
#include "atmel_start_pins.h"

/* Public Definitions */
#define MUTE_SET_MUTE_PIN		MUTE_set_level(true)
#define MUTE_CLEAR_MUTE_PIN		MUTE_set_level(false)

/* Public Functions */
extern void app_MUTEInit(void);
extern void app_MUTETask(void);
extern void app_MUTECtrl(void);

#endif /* APP_MUTE_H_ */