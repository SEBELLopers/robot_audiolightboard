/*
 * app_Strobe.h
 *
 * Created: 25/08/2019 11:17:34 p. m.
 *  Author: OmarSevilla
 */ 


#ifndef APP_STROBESEQ_H_
#define APP_STROBESEQ_H_
#include "atmel_start_pins.h"

/* Public definitions */
#define STROBESEQ_TOGGLE_STROBE1_PIN	STROBE_1_toggle_level()
#define STROBESEQ_CLEAR_STROBE1_PIN		STROBE_1_set_level(false)


#define STROBESEQ_TOGGLE_STROBE2_PIN	STROBE_2_toggle_level()
#define STROBESEQ_CLEAR_STROBE2_PIN		STROBE_2_set_level(false)

/* Public Variables */
extern unsigned char rub_STROBETick;

/* Public functions */
extern void app_STROBESeqInit(void);
extern void app_STROBESeqTask(void);
extern void app_STROBESeqCtrl(void);

#endif /* APP_STROBESEQ_H_ */