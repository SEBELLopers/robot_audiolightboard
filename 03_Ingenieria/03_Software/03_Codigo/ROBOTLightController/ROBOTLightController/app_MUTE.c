/*
 * app_MUTE.c
 *
 * Created: 25/08/2019 11:57:35 p. m.
 *  Author: OmarSevilla
 */ 
#include "app_MUTE.h"

/* Private Variables */
static unsigned char rub_MUTEEnable;

/******************* Public Functions ***********************/

/******************************************************
* Function Name: app_MUTEInit
* Description: This function init the MUTE module
* Parameters: void
* Return: void
*******************************************************/
void app_MUTEInit(void)
{
	/* Default value for strobe */
	rub_MUTEEnable = false;
}

/******************************************************
* Function Name: app_MUTETask
* Description: This function execute MUTE if enable.
This function shall called periodically
* Parameters: void
* Return: void
*******************************************************/
void app_MUTETask(void)
{
	if(false != rub_MUTEEnable)
	{//MUTE seq enable
		MUTE_SET_MUTE_PIN;
	}
	else
	{//MUTE seq disable
		MUTE_CLEAR_MUTE_PIN;
	}
}

/******************************************************
* Function Name: app_MUTECtrl
* Description: Activate or disable MUTE
* Parameters: void
* Return: void
*******************************************************/
void app_MUTECtrl(void)
{
	/* Check Mute status */
	if(rub_MUTEEnable != true)
	{//Mute disable
		//Enable strobe
		rub_MUTEEnable = true;
	}
	else
	{//Mute enabled
		//Disable mute
		rub_MUTEEnable = false;
	}
}