/*
 * app_SWPWM.h
 *
 * Created: 08/01/2020 08:49:41 p.m.
 *  Author: uidj2522
 */ 


#ifndef APP_SWPWM_H_
#define APP_SWPWM_H_
#include "atmel_start_pins.h"

#define APP_SWPWM_CH0_SET_HIGH	LED_R_set_level(true)
#define APP_SWPWM_CH0_SET_LOW	LED_R_set_level(false)

#define APP_SWPWM_CH1_SET_HIGH	LED_G_set_level(true)
#define APP_SWPWM_CH1_SET_LOW	LED_G_set_level(false)

#define APP_SWPWM_CH2_SET_HIGH	LED_B_set_level(true)
#define APP_SWPWM_CH2_SET_LOW	LED_B_set_level(false)

 #define APP_SWPWM_N_CHANNELS		3u
 #define APP_SWPWM_MAX_CLOCKCOUNT	100u

 extern void app_SWPWMInit(void);
 extern void app_SWPWM_Task(void);
 extern void app_SWPWM_IncreaseChVal(unsigned char lub_Channel);
 extern void app_SWPWM_DecreaseChVal(unsigned char lub_Channel);
 extern unsigned char app_SWPWM_GetCHValue(unsigned char lub_Channel);


#endif /* APP_SWPWM_H_ */