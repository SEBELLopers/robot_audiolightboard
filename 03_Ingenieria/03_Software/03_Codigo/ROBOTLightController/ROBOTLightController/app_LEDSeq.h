/*
 * app_LEDSeq.h
 *
 * Created: 25/08/2019 10:20:49 p. m.
 *  Author: OmarSevilla
 */ 


#ifndef APP_LEDSEQ_H_
#define APP_LEDSEQ_H_
#include "atmel_start_pins.h"

typedef enum
{
	LED_OFF,
	LED_PWM_V3,
	LED_PWM_V2,
	LED_PWM_V1,
	LED_ALL_SEQ,
	LED_TOG_SEQ,
	LED_STR_SEQ,
	LED_RED,
	LED_MAGENTA,
	LED_BLUE,
	LED_CYAN,
	LED_GREEN,
	LED_YELLOW,
	LED_WHITE,
	LED_RED_TOGGLE,
	LED_MAGENTA_TOGGLE,
	LED_BLUE_TOGGLE,
	LED_CYAN_TOOGLE,
	LED_GREEN_TOGGLE,
	LED_YELLOW_TOGGLE,
	LED_WHITE_TOGGLE,
	LED_RED_STROBE,
	LED_MAGENTA_STROBE,
	LED_BLUE_STROBE,
	LED_CYAN_STROBE,
	LED_GREEN_STROBE,
	LED_YELLOW_STROBE,
	LED_WHITE_STROBE,
	N_LED_SEQ
}T_LEDSEQ;

/* Public Macros */
#define LEDSEQ_SET_RED_PIN		LED_R_set_level(true)
#define LEDSEQ_CLEAR_RED_PIN	LED_R_set_level(false)
#define LEDSEQ_TOGGLE_RED_PIN	LED_R_toggle_level()

#define LEDSEQ_SET_GREEN_PIN	LED_G_set_level(true)
#define LEDSEQ_CLEAR_GREEN_PIN	LED_G_set_level(false)
#define LEDSEQ_TOGGLE_GREEN_PIN	LED_G_toggle_level()

#define LEDSEQ_SET_BLUE_PIN		LED_B_set_level(true)
#define LEDSEQ_CLEAR_BLUE_PIN	LED_B_set_level(false)
#define LEDSEQ_TOGGLE_BLUE_PIN	LED_B_toggle_level()

/* Public Variables */
extern unsigned char rub_LEDSeqTick;

/* Public Functions */
extern void app_LEDSeqTask(void);
extern void app_LEDNextSeq(void);
extern void app_LEDSeqInit(void);
extern T_LEDSEQ app_LEDSeq_GetSeq(void);

#endif /* APP_LEDSEQ_H_ */