/*
 * app_LASERSeq.h
 *
 * Created: 25/08/2019 11:50:36 p. m.
 *  Author: OmarSevilla
 */ 


#ifndef APP_LASERSEQ_H_
#define APP_LASERSEQ_H_
#include "atmel_start_pins.h"

/* Public Definitions */
#define LASERSEQ_SET_LASER_PIN		LASER_set_level(true)
#define LASERSEQ_CLEAR_LASER_PIN	LASER_set_level(false)

/* Public Functions */
extern void app_LASERSeqInit(void);
extern void app_LASERSeqTask(void);
extern void app_LASERSeqCtrl(void);

#endif /* APP_LASERSEQ_H_ */