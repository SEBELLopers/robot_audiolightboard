/*
* app_LEDSeq.c
*
* Created: 25/08/2019 10:20:33 p. m.
*  Author: OmarSevilla
*/


#include "app_LEDSeq.h"
#include "app_SWPWM.h"

/* Private Types */

typedef enum
{
	//BLACK,
	RED,
	GREEN,
	BLUE,
	YELLOW,
	CYAN,
	PURPLE,
	WHITE,
	N_ALL_SEQ
}T_ALLSEQ;

typedef enum
{
	PWM_RED,
	PWM_MAGENTA,
	PWM_BLUE,
	PWM_CYAN,
	PWM_GREEN,
	PWM_YELLOW,
	PWM_WHITE
}T_LEDPWM_SEQ;

/* Private Definitions */
#define LED_PWM_RED_CH	0u
#define LED_PWM_GREEN_CH	1u
#define LED_PWM_BLUE_CH	2u

#define LEDSEQ_PWM_V1_TIME	(10u)	//20ms
#define LEDSEQ_PWM_V2_TIME	(5u)	//10ms
#define LEDSEQ_PWM_V3_TIME	(1u)	//5ms
#define LEDSEQ_TOGGLE_TIME	(250u) //250ms
#define LEDSEQ_ALLSEQ_TIME	(1000u) //1000ms
#define LEDSEQ_STROBE_TIME	(50u) //50ms

#define LEDSEQ_TOGGLE_N_TIMES	(8u) //8 toggles for change

/* Private Variables */
static T_LEDSEQ re_LEDSeq;
static T_LEDSEQ re_LEDToggleSeq;
static T_ALLSEQ re_ALLSeq;
static unsigned int ruw_SeqCounter;
static unsigned char ruw_ToggleCounter;
static T_LEDPWM_SEQ re_LEDPWMSeq;
static unsigned char	rub_LEDStrobeFlag;

/* Public Variables */
unsigned char rub_LEDSeqTick;

/******************* Private Functions ***********************/
static void app_LEDSeqAllSeq(void);
static void app_LED_PWMSeq(void);

/******************************************************
* Function Name: app_LEDSeqAllSeq
* Description: This function execute the ALL seq SM
* Parameters: void
* Return: void
*******************************************************/
static void app_LEDSeqAllSeq(void)
{
	/* All seq SM */
	switch (re_ALLSeq)
	{
		default:
		//case BLACK:
		//{
			////Clear all outputs
			//LEDSEQ_CLEAR_RED_PIN;
			//LEDSEQ_CLEAR_GREEN_PIN;
			//LEDSEQ_CLEAR_BLUE_PIN;
			//
			///* Set next state */
			//re_ALLSeq = RED;
		//}break;
		case RED:
		{
			//Clear GB outputs
			LEDSEQ_CLEAR_GREEN_PIN;
			LEDSEQ_CLEAR_BLUE_PIN;
			
			//Set R Output
			LEDSEQ_SET_RED_PIN;

			/* Set next state */
			re_ALLSeq = GREEN;
			
		}break;
		case GREEN:
		{
			//Clear RB outputs
			LEDSEQ_CLEAR_RED_PIN;
			LEDSEQ_CLEAR_BLUE_PIN;
			
			//Set G Output
			LEDSEQ_SET_GREEN_PIN;
			
			/* Set next state */
			re_ALLSeq = BLUE;
			
		}break;
		case BLUE:
		{
			//Clear RG outputs
			LEDSEQ_CLEAR_RED_PIN;
			LEDSEQ_CLEAR_GREEN_PIN;
			
			//Set B Output
			LEDSEQ_SET_BLUE_PIN;
			
			/* Set next state */
			re_ALLSeq = YELLOW;
			
		}break;
		case YELLOW:
		{
			//Clear B output
			LEDSEQ_CLEAR_BLUE_PIN;
			
			//Set RG Outputs
			LEDSEQ_SET_RED_PIN;
			LEDSEQ_SET_GREEN_PIN;
			
			/* Set next state */
			re_ALLSeq = CYAN;
			
		}break;
		case CYAN:
		{
			//Clear R output
			LEDSEQ_CLEAR_RED_PIN;
			
			//Set GB Outputs
			LEDSEQ_SET_GREEN_PIN;
			LEDSEQ_SET_BLUE_PIN;
			
			/* Set next state */
			re_ALLSeq = PURPLE;
			
		}break;
		case PURPLE:
		{
			//Clear G output
			LEDSEQ_CLEAR_GREEN_PIN;
			
			//Set RB Outputs
			LEDSEQ_SET_RED_PIN;
			LEDSEQ_SET_BLUE_PIN;
			
			/* Set next state */
			re_ALLSeq = WHITE;
			
		}break;
		case WHITE:
		{
			//Set RGB Outputs
			LEDSEQ_SET_RED_PIN;
			LEDSEQ_SET_GREEN_PIN;
			LEDSEQ_SET_BLUE_PIN;
			
			/* Set next state */
			re_ALLSeq = RED;
			
		}break;
	}
}

/******************* Public Functions ***********************/

/******************************************************
* Function Name: app_LEDSeqInit
* Description: This function initi the LED Seq module
* Parameters: void
* Return: void
*******************************************************/
void app_LEDSeqInit(void)
{
	/* Clear Tick */
	rub_LEDSeqTick = false;
	/* Default Seq at init */
	re_LEDSeq = LED_OFF;
	/* Clear Seq Counter */
	ruw_SeqCounter = 0;
	/* Toggle Counter Clear */
	ruw_ToggleCounter = 0u;
	/* Clear all seq at init */
	re_ALLSeq = RED;
	re_LEDToggleSeq = LED_RED_TOGGLE;
	re_LEDPWMSeq = PWM_RED;
	rub_LEDStrobeFlag = false;
}

/******************************************************
* Function Name: app_LEDSeqTask
* Description: This function execute LED sequence selected.
This function shall called periodically
* Parameters: void
* Return: void
*******************************************************/
void app_LEDSeqTask(void)
{
	T_LEDSEQ le_LEDSeq;

	if(rub_LEDSeqTick)
	{//Tick available
		rub_LEDSeqTick = false;
		
		//Is toogle seq
		if(re_LEDSeq != LED_TOG_SEQ && re_LEDSeq != LED_STR_SEQ)
		{//No toggle
			le_LEDSeq = re_LEDSeq;
		}
		else
		{
			if(re_LEDSeq == LED_STR_SEQ)
			{
				rub_LEDStrobeFlag = true;
			}
			else
			{
				rub_LEDStrobeFlag = false;
			}
			//Load the next toggle seq
			le_LEDSeq = re_LEDToggleSeq;

			//Check if n toggles has been passed
			if (ruw_ToggleCounter >= LEDSEQ_TOGGLE_N_TIMES)
			{
				//Reset Toggle Counter
				ruw_ToggleCounter = 0u;
				
				if(re_LEDToggleSeq >= (N_LED_SEQ - 1))
				{
					re_LEDToggleSeq = LED_RED_TOGGLE;
				}
				else
				{
					re_LEDToggleSeq++;
					//Clear all outputs
					LEDSEQ_CLEAR_RED_PIN;
					LEDSEQ_CLEAR_GREEN_PIN;
					LEDSEQ_CLEAR_BLUE_PIN;
				}
			}
			else
			{
				if(ruw_SeqCounter >= LEDSEQ_TOGGLE_TIME || 
				(rub_LEDStrobeFlag == true && ruw_SeqCounter >= LEDSEQ_STROBE_TIME))
				{
					ruw_ToggleCounter++;
				}
				else
				{
					//Do nothing
				}
			}
		}

		/* LEDSeq SM */
		switch (le_LEDSeq)
		{
			default:
			case LED_OFF:
			{
				//Clear all outputs
				LEDSEQ_CLEAR_RED_PIN;
				LEDSEQ_CLEAR_GREEN_PIN;
				LEDSEQ_CLEAR_BLUE_PIN;
			}break;
			case LED_PWM_V1:
			{
				if(ruw_SeqCounter >= LEDSEQ_PWM_V1_TIME)
				{
					//Restart sequence counter
					ruw_SeqCounter = 0u;
					//Execute PWM Seq
					app_LED_PWMSeq();
				}
				else
				{
					//Increase Seq Counter
					ruw_SeqCounter++;
				}
			}break;
			case LED_PWM_V2:
			{
				if(ruw_SeqCounter >= LEDSEQ_PWM_V2_TIME)
				{
					//Restart sequence counter
					ruw_SeqCounter = 0u;
					//Execute PWM Seq
					app_LED_PWMSeq();
				}
				else
				{
					//Increase Seq Counter
					ruw_SeqCounter++;
				}
			}break;
			case LED_PWM_V3:
			{
				if(ruw_SeqCounter >= LEDSEQ_PWM_V3_TIME)
				{
					//Restart sequence counter
					ruw_SeqCounter = 0u;
					//Execute PWM Seq
					app_LED_PWMSeq();
				}
				else
				{
					//Increase Seq Counter
					ruw_SeqCounter++;
				}
			}break;
			case LED_MAGENTA:
			{
				//Clear G output
				LEDSEQ_CLEAR_GREEN_PIN;
				
				//Set RB Outputs
				LEDSEQ_SET_RED_PIN;
				LEDSEQ_SET_BLUE_PIN;
			}break;
			case LED_CYAN:
			{
				//Clear R output
				LEDSEQ_CLEAR_RED_PIN;
				
				//Set GB Outputs
				LEDSEQ_SET_GREEN_PIN;
				LEDSEQ_SET_BLUE_PIN;
			}break;
			case LED_YELLOW:
			{
				//Clear B output
				LEDSEQ_CLEAR_BLUE_PIN;
				
				//Set RG Outputs
				LEDSEQ_SET_RED_PIN;
				LEDSEQ_SET_GREEN_PIN;
			}break;
			case LED_RED:
			{
				//Clear GB outputs
				LEDSEQ_CLEAR_GREEN_PIN;
				LEDSEQ_CLEAR_BLUE_PIN;
				
				//Set R Output
				LEDSEQ_SET_RED_PIN;
			}break;
			case LED_RED_STROBE:
			{
				rub_LEDStrobeFlag = true;
			}
			case LED_RED_TOGGLE:
			{
				//Clear GB outputs
				LEDSEQ_CLEAR_GREEN_PIN;
				LEDSEQ_CLEAR_BLUE_PIN;
				
				/* Check Sequence Counter */
				if (ruw_SeqCounter >= LEDSEQ_TOGGLE_TIME ||
				(rub_LEDStrobeFlag == true && ruw_SeqCounter >= LEDSEQ_STROBE_TIME))
				{
					//Restart sequence counter
					ruw_SeqCounter = 0u;
					//Toggle R Output
					LEDSEQ_TOGGLE_RED_PIN;
				}
				else
				{
					//Increase Sequence counter
					ruw_SeqCounter++;
				}
				
			}break;
			case LED_MAGENTA_STROBE:
			{
				rub_LEDStrobeFlag = true;
			}
			case LED_MAGENTA_TOGGLE:
			{
				//Clear G output
				LEDSEQ_CLEAR_GREEN_PIN;
				
				/* Check Sequence Counter */
				if (ruw_SeqCounter >= LEDSEQ_TOGGLE_TIME||
				(rub_LEDStrobeFlag == true && ruw_SeqCounter >= LEDSEQ_STROBE_TIME))
				{
					//Restart sequence counter
					ruw_SeqCounter = 0u;
					//Toggle R Output
					LEDSEQ_TOGGLE_RED_PIN;
					//Set B Out as R Out
					LEDSEQ_TOGGLE_BLUE_PIN;
				}
				else
				{
					//Increase Sequence counter
					ruw_SeqCounter++;
				}
			}break;
			case LED_CYAN_STROBE:
			{
				rub_LEDStrobeFlag = true;
			}
			case LED_CYAN_TOOGLE:
			{
				//Clear R output
				LEDSEQ_CLEAR_RED_PIN;
				
				/* Check Sequence Counter */
				if (ruw_SeqCounter >= LEDSEQ_TOGGLE_TIME||
				(rub_LEDStrobeFlag == true && ruw_SeqCounter >= LEDSEQ_STROBE_TIME))
				{
					//Restart sequence counter
					ruw_SeqCounter = 0u;
					//Toggle G Output
					LEDSEQ_TOGGLE_GREEN_PIN;
					//Set B Out as G Out
					LEDSEQ_TOGGLE_BLUE_PIN;
				}
				else
				{
					//Increase Sequence counter
					ruw_SeqCounter++;
				}
			}break;
			case LED_YELLOW_STROBE:
			{
				rub_LEDStrobeFlag = true;
			}
			case LED_YELLOW_TOGGLE:
			{
				//Clear B output
				LEDSEQ_CLEAR_BLUE_PIN;
				
				/* Check Sequence Counter */
				if (ruw_SeqCounter >= LEDSEQ_TOGGLE_TIME||
				(rub_LEDStrobeFlag == true && ruw_SeqCounter >= LEDSEQ_STROBE_TIME))
				{
					//Restart sequence counter
					ruw_SeqCounter = 0u;
					//Toggle G Output
					LEDSEQ_TOGGLE_GREEN_PIN;
					//Set R Out as G Out
					LEDSEQ_TOGGLE_RED_PIN;
				}
				else
				{
					//Increase Sequence counter
					ruw_SeqCounter++;
				}
			}break;
			case LED_WHITE_STROBE:
			{
				rub_LEDStrobeFlag = true;
			}
			case LED_WHITE_TOGGLE:
			{
				/* Check Sequence Counter */
				if (ruw_SeqCounter >= LEDSEQ_TOGGLE_TIME||
				(rub_LEDStrobeFlag == true && ruw_SeqCounter >= LEDSEQ_STROBE_TIME))
				{
					//Restart sequence counter
					ruw_SeqCounter = 0u;
					//Toggle G Output
					LEDSEQ_TOGGLE_GREEN_PIN;
					//Set RB Out as G Out
					LEDSEQ_TOGGLE_RED_PIN;
					LEDSEQ_TOGGLE_BLUE_PIN;
				}
				else
				{
					//Increase Sequence counter
					ruw_SeqCounter++;
				}
			}break;
			case LED_GREEN:
			{
				//Clear RB outputs
				LEDSEQ_CLEAR_RED_PIN;
				LEDSEQ_CLEAR_BLUE_PIN;
				
				//Set G Output
				LEDSEQ_SET_GREEN_PIN;
			}break;
			case LED_GREEN_STROBE:
			{
				rub_LEDStrobeFlag = true;
			}
			case LED_GREEN_TOGGLE:
			{
				//Clear RB outputs
				LEDSEQ_CLEAR_RED_PIN;
				LEDSEQ_CLEAR_BLUE_PIN;
				
				/* Check Sequence Counter */
				if (ruw_SeqCounter >= LEDSEQ_TOGGLE_TIME||
				(rub_LEDStrobeFlag == true && ruw_SeqCounter >= LEDSEQ_STROBE_TIME))
				{
					//Restart sequence counter
					ruw_SeqCounter = 0u;
					//Toggle G Output
					LEDSEQ_TOGGLE_GREEN_PIN;
				}
				else
				{
					//Increase Sequence counter
					ruw_SeqCounter++;
				}
			}break;
			case LED_BLUE:
			{
				//Clear RG outputs
				LEDSEQ_CLEAR_RED_PIN;
				LEDSEQ_CLEAR_GREEN_PIN;
				
				//Set B Output
				LEDSEQ_SET_BLUE_PIN;
			}break;
			case LED_BLUE_STROBE:
			{
				rub_LEDStrobeFlag = true;
			}
			case LED_BLUE_TOGGLE:
			{
				//Clear RG outputs
				LEDSEQ_CLEAR_RED_PIN;
				LEDSEQ_CLEAR_GREEN_PIN;
				
				/* Check Sequence Counter */
				if (ruw_SeqCounter >= LEDSEQ_TOGGLE_TIME||
				(rub_LEDStrobeFlag == true && ruw_SeqCounter >= LEDSEQ_STROBE_TIME))
				{
					//Restart sequence counter
					ruw_SeqCounter = 0u;
					//Toggle B Output
					LEDSEQ_TOGGLE_BLUE_PIN;
				}
				else
				{
					//Increase Sequence counter
					ruw_SeqCounter++;
				}
			}break;
			case LED_WHITE:
			{
				//Set RGB Outputs
				LEDSEQ_SET_RED_PIN;
				LEDSEQ_SET_GREEN_PIN;
				LEDSEQ_SET_BLUE_PIN;
			}break;
			case LED_ALL_SEQ:
			{
				/* Check Sequence Counter */
				if (ruw_SeqCounter >= LEDSEQ_ALLSEQ_TIME)
				{
					//Restart sequence counter
					ruw_SeqCounter = 0u;
					//Execute all seq
					app_LEDSeqAllSeq();
				}
				else
				{
					//Increase Sequence counter
					ruw_SeqCounter++;
				}
			}break;
		}

	}
	else
	{//Tick not available
		/* Do nothing */
	}
}

/******************************************************
* Function Name: app_LEDNextSeq
* Description: Select the next seq in the list
* Parameters: void
* Return: void
*******************************************************/
void app_LEDNextSeq(void)
{
	//Clear all outputs
	LEDSEQ_CLEAR_RED_PIN;
	LEDSEQ_CLEAR_GREEN_PIN;
	LEDSEQ_CLEAR_BLUE_PIN;
	//Restar Strobe Flag
	rub_LEDStrobeFlag = false;
	//Restart Sequence counter
	ruw_SeqCounter = 0u;
	/* Toggle Counter Clear */
	ruw_ToggleCounter = 0u;
	//Restart All Seq State
	re_ALLSeq = RED;
	//Restart PWM Seq State
	re_LEDPWMSeq = PWM_RED;
	//Restart Toggle Seq State
	re_LEDToggleSeq = LED_RED_TOGGLE;
	/* Check if the last item of the list has been reached */
	if(re_LEDSeq >= (N_LED_SEQ - 1u))
	{//Last item reached
		//Reset LEDSeq SM
		re_LEDSeq = LED_OFF;
	}
	else
	{//Last item not reached yet
		//Select next LEDSeq
		re_LEDSeq++;
	}
}

/******************************************************
* Function Name: app_LEDNextSeq
* Description: Select the next seq in the list
* Parameters: void
* Return: void
*******************************************************/
static void app_LED_PWMSeq(void)
{
	switch (re_LEDPWMSeq)
	{
		default:
		case PWM_RED:
		{
			/* Decrease GB Channels */
			app_SWPWM_DecreaseChVal(LED_PWM_GREEN_CH);
			app_SWPWM_DecreaseChVal(LED_PWM_BLUE_CH);

			/* Increase R Channel */
			app_SWPWM_IncreaseChVal(LED_PWM_RED_CH);

			/* If MAX Red value has been reached then change to next seq */
			if((app_SWPWM_GetCHValue(LED_PWM_RED_CH) == APP_SWPWM_MAX_CLOCKCOUNT) &&
			(app_SWPWM_GetCHValue(LED_PWM_GREEN_CH) == 0u) &&
			(app_SWPWM_GetCHValue(LED_PWM_BLUE_CH) == 0u))
			{
				re_LEDPWMSeq++;
			}
			else
			{
				/* Do Nothing */
			}
		}break;
		case PWM_MAGENTA:
		{
			/* Decrease G Channel */
			app_SWPWM_DecreaseChVal(LED_PWM_GREEN_CH);

			/* Increase RB Channels */
			app_SWPWM_IncreaseChVal(LED_PWM_RED_CH);
			app_SWPWM_IncreaseChVal(LED_PWM_BLUE_CH);

			/* If MAX Red value has been reached then change to next seq */
			if((app_SWPWM_GetCHValue(LED_PWM_RED_CH) == APP_SWPWM_MAX_CLOCKCOUNT) &&
			(app_SWPWM_GetCHValue(LED_PWM_GREEN_CH) == 0u) &&
			(app_SWPWM_GetCHValue(LED_PWM_BLUE_CH) == APP_SWPWM_MAX_CLOCKCOUNT))
			{
				re_LEDPWMSeq++;
			}
			else
			{
				/* Do Nothing */
			}
		}break;
		case PWM_BLUE:
		{
			/* Decrease RG Channels */
			app_SWPWM_DecreaseChVal(LED_PWM_GREEN_CH);
			app_SWPWM_DecreaseChVal(LED_PWM_RED_CH);

			/* Increase R Channel */
			app_SWPWM_IncreaseChVal(LED_PWM_BLUE_CH);

			/* If MAX Red value has been reached then change to next seq */
			if((app_SWPWM_GetCHValue(LED_PWM_RED_CH) == 0u) &&
			(app_SWPWM_GetCHValue(LED_PWM_GREEN_CH) == 0u) &&
			(app_SWPWM_GetCHValue(LED_PWM_BLUE_CH) == APP_SWPWM_MAX_CLOCKCOUNT))
			{
				re_LEDPWMSeq++;
			}
			else
			{
				/* Do Nothing */
			}
		}break;
		case PWM_CYAN:
		{
			/* Decrease R Channel */
			app_SWPWM_DecreaseChVal(LED_PWM_RED_CH);

			/* Increase GB Channels */
			app_SWPWM_IncreaseChVal(LED_PWM_GREEN_CH);
			app_SWPWM_IncreaseChVal(LED_PWM_BLUE_CH);

			/* If MAX Red value has been reached then change to next seq */
			if((app_SWPWM_GetCHValue(LED_PWM_RED_CH) == 0u) &&
			(app_SWPWM_GetCHValue(LED_PWM_GREEN_CH) == APP_SWPWM_MAX_CLOCKCOUNT) &&
			(app_SWPWM_GetCHValue(LED_PWM_BLUE_CH) == APP_SWPWM_MAX_CLOCKCOUNT))
			{
				re_LEDPWMSeq++;
			}
			else
			{
				/* Do Nothing */
			}
		}break;
		case PWM_GREEN:
		{
			/* Decrease RB Channels */
			app_SWPWM_DecreaseChVal(LED_PWM_BLUE_CH);
			app_SWPWM_DecreaseChVal(LED_PWM_RED_CH);

			/* Increase R Channel */
			app_SWPWM_IncreaseChVal(LED_PWM_GREEN_CH);

			/* If MAX Red value has been reached then change to next seq */
			if((app_SWPWM_GetCHValue(LED_PWM_RED_CH) == 0u) &&
			(app_SWPWM_GetCHValue(LED_PWM_GREEN_CH) == APP_SWPWM_MAX_CLOCKCOUNT) &&
			(app_SWPWM_GetCHValue(LED_PWM_BLUE_CH) == 0u))
			{
				re_LEDPWMSeq++;
			}
			else
			{
				/* Do Nothing */
			}
		}break;
		case PWM_YELLOW:
		{
			/* Decrease B Channel */
			app_SWPWM_DecreaseChVal(LED_PWM_BLUE_CH);

			/* Increase GB Channels */
			app_SWPWM_IncreaseChVal(LED_PWM_GREEN_CH);
			app_SWPWM_IncreaseChVal(LED_PWM_RED_CH);

			/* If MAX Red value has been reached then change to next seq */
			if((app_SWPWM_GetCHValue(LED_PWM_RED_CH) == APP_SWPWM_MAX_CLOCKCOUNT) &&
			(app_SWPWM_GetCHValue(LED_PWM_GREEN_CH) == APP_SWPWM_MAX_CLOCKCOUNT) &&
			(app_SWPWM_GetCHValue(LED_PWM_BLUE_CH) == 0u))
			{
				re_LEDPWMSeq++;
			}
			else
			{
				/* Do Nothing */
			}
		}break;
		case PWM_WHITE:
		{

			/* Increase RGB Channels */
			app_SWPWM_IncreaseChVal(LED_PWM_BLUE_CH);
			app_SWPWM_IncreaseChVal(LED_PWM_GREEN_CH);
			app_SWPWM_IncreaseChVal(LED_PWM_RED_CH);

			/* If MAX White value has been reached then change to next seq */
			if((app_SWPWM_GetCHValue(LED_PWM_RED_CH) == APP_SWPWM_MAX_CLOCKCOUNT) &&
			(app_SWPWM_GetCHValue(LED_PWM_GREEN_CH) == APP_SWPWM_MAX_CLOCKCOUNT) &&
			(app_SWPWM_GetCHValue(LED_PWM_BLUE_CH) == APP_SWPWM_MAX_CLOCKCOUNT))
			{
				re_LEDPWMSeq = PWM_RED;
			}
			else
			{
				/* Do Nothing */
			}
		}break;
	}
}

/******************************************************
* Function Name: app_LEDSeq_GetSeq
* Description: Get the actual state
* Parameters: void
* Return: State
*******************************************************/
T_LEDSEQ app_LEDSeq_GetSeq(void)
{
	return re_LEDSeq;
}