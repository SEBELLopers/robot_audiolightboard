/*
 * app_SWPWM.c
 *
 * Created: 08/01/2020 08:49:20 p.m.
 *  Author: uidj2522
 */ 

 #include "app_SWPWM.h"
 #include "string.h"

 static unsigned char	raub_ChannelValue[APP_SWPWM_N_CHANNELS];
 static unsigned char	rub_ClockCounter;

/******************************************************
* Function Name: app_SWPWMInit
* Description: This function init the SW PWM module
* Parameters: void
* Return: void
*******************************************************/
void app_SWPWMInit(void)
{
	/* Init Counter */
	rub_ClockCounter = 0u;

	/* Inits in 0u */
	memset(raub_ChannelValue,0u,APP_SWPWM_N_CHANNELS);
}

/******************************************************
* Function Name: app_SWPWM_Task
* Description: This function execute the SW PWM module
* Parameters: void
* Return: void
*******************************************************/
void app_SWPWM_Task(void)
{
	/* Check the channels values against the clock */
	if(raub_ChannelValue[0] > rub_ClockCounter)
	{
		APP_SWPWM_CH0_SET_HIGH;
	}
	else
	{
		APP_SWPWM_CH0_SET_LOW;
	}
	if(raub_ChannelValue[1] > rub_ClockCounter)
	{
		APP_SWPWM_CH1_SET_HIGH;
	}
	else
	{
		APP_SWPWM_CH1_SET_LOW;
	}
	if(raub_ChannelValue[2] > rub_ClockCounter)
	{
		APP_SWPWM_CH2_SET_HIGH;
	}
	else
	{
		APP_SWPWM_CH2_SET_LOW;
	}

	/* Increase Clock Counter for next time. */
	if(rub_ClockCounter >= APP_SWPWM_MAX_CLOCKCOUNT)
	{
		rub_ClockCounter = 0;
	}
	else
	{
		rub_ClockCounter++;
	}
}

/******************************************************
* Function Name: app_SWPWM_IncreaseChVal
* Description: This function increase the SW PWM CH selected
* Parameters: Channel to be increased
* Return: void
*******************************************************/
void app_SWPWM_IncreaseChVal(unsigned char lub_Channel)
{
	if (raub_ChannelValue[lub_Channel] < APP_SWPWM_MAX_CLOCKCOUNT)
	{
		raub_ChannelValue[lub_Channel]++;
	}
	else
	{
		/* Do nothing */
	}
}

/******************************************************
* Function Name: app_SWPWM_DecreaseChVal
* Description: This function decrease the SW PWM CH selected
* Parameters: Channel to be decreased
* Return: void
*******************************************************/
void app_SWPWM_DecreaseChVal(unsigned char lub_Channel)
{
	if(raub_ChannelValue[lub_Channel] > 0u)
	{
		raub_ChannelValue[lub_Channel]--;
	}
	else
	{
		/* Do Nothing */
	}
}

/******************************************************
* Function Name: app_SWPWM_GetCHValue
* Description: This function decrease the SW PWM CH selected
* Parameters: Channel
* Return: Channel Value
*******************************************************/
unsigned char app_SWPWM_GetCHValue(unsigned char lub_Channel)
{
	return raub_ChannelValue[lub_Channel];
}