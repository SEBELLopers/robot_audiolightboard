/*
 * ButtonDebounce.h
 *
 *  Created on: 24 ago 2019
 *      Author: uidv7995
 */

#ifndef APP_DEBOUNCE_H_
#define APP_DEBOUNCE_H_

/********************************************************************************************
 *Error Codes
 ********************************************************************************************/
#define NOK_INVALID_BUTTON_ID 0xAA

/********************************************************************************************
 *Configuration Defines
 ********************************************************************************************/
#define APP_DBNC_LONGPRESS
#define APP_DBNC_VARIABLELONGPRESS
#define APP_DBNC_CLBCK_ENABLED
#define PIN_ACTIVE_INVERSE 0x0u
#define PIN_ACTIVE 0x01u

/********************************************************************************************
 *Declaration of types utilized for External modules
 ********************************************************************************************/

typedef enum{
	IDLE = 0x00u,
	PRESSED,
	LONGPRESSED,
	DEBOUNCING
}T_BUTTON_STS; /*States for the Button State Machine*/

/********************************************************************************************
 *Configuration of the Button Debounce
 ********************************************************************************************/

/*Declarations of the Button IDS*/
typedef enum{
	BUTTON_0 = 0x00u,
	BUTTON_1 ,
	BUTTON_2,
	BUTTON_3,
	NB_BUTTONS
}T_BUTTON_ID;

/*Externs of the Callback Functions*/
extern void cbk_Button0(T_BUTTON_STS l_ButtonID_Sts);
extern void cbk_Button1(T_BUTTON_STS l_ButtonID_Sts);
extern void cbk_Button2(T_BUTTON_STS l_ButtonID_Sts);
extern void cbk_Button3(T_BUTTON_STS l_ButtonID_Sts);

/********************************************************************************************
 *External Variables
 ********************************************************************************************/
extern unsigned char r_taskTickCounter; /*Variable that provides Tick count to execute the task*/
/********************************************************************************************
 *External Interfaces
 ********************************************************************************************/
extern T_BUTTON_STS Get_Button_Status(T_BUTTON_ID l_ButtonID); /*Returns the Status of the Button to the Application Layers*/
extern void app_Debounce_Task(void);
#endif /* APP_DEBOUNCE_H_ */
